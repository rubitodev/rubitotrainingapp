import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
const Task = (props) => (
    <View style={styles.taskWrapper}>
        <View>
            
            {/* <View style={styles.verticalLine}></View> */}
            <Text style={styles.task}>{props.key}</Text>
            <Text style={styles.task}>{props.text}</Text>
        </View>
        <Icon
            name="trash"
            size={30}
            color="black"
            style={{ marginLeft: 'auto', padding:10}}
            onPress={props.delete}
        />
    </View>
)

export default Task

const styles = StyleSheet.create({
    taskWrapper: {
        marginTop: '5%',
        flexDirection: 'row',
        borderColor: 'black',
        borderWidth: 1,
        width: '98%',
        alignItems: 'flex-end',
        height:80,
        paddingRight:20,
    },
    task: {
        paddingBottom: 20,
        paddingLeft: 10,
        marginTop: 6,
        borderColor: 'black',
        fontSize: 17,
        fontWeight: '600',
        color: 'black',
        justifyContent:'flex-end'
    },
    verticalLine: {
        borderBottomColor: 'white',
        borderBottomWidth: 4,
        marginLeft: 10,
        width: '100%',
        position: 'absolute',
        marginTop: 15
    }
})
