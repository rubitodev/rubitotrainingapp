import React, { useState,useRef, useEffect } from 'react';
import uri from '../../api';
import {RNCamera} from 'react-native-camera';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TouchableOpacity, View, Text, Image, Modal, TextInput,ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

const editAccount =({navigation, route}) => {
    let input = useRef(null)
    let camera = useRef(null)
    const [editable, setEditable] = useState(false)
    const [token, setToken] = useState('')
    const [name, setName] = useState(route.params.name)
    const [email, setEmail] = useState(route.params.email)
    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toggleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async() => {
        const options = {quality: 0.5, base64: true}
        if(camera){
            const data = await camera.current.takePictureAsync(options)
            // console.log("editAccount -> data", data)
            setPhoto(data)
            setIsVisible(false)
        }
    }


    useEffect(() => {
        const getToken = async() => {
            try {
                const token = await AsyncStorage.getItem('token')
                if(token !== null){
                    setToken(token)
                }
            } catch (error) {
                console.log(err);
                
            }
        }
        getToken()
    }, [])
    
    const editData = () => {
        setEditable(!editable)
    }

    const onSavePress = () => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('photo', {
            uri: photo.uri,
            name: 'photo.jpg',
            type:'image/jpg'
        })
        Axios.post(`${uri.api}/profile/update-profile`, formData,{
            timeout: 20000,
            headers: {
                'Authorization' : "Bearer" + token,
                Accept: 'application/json',
                'Content-type' : 'multipart/form-data'
            }
        })
        .then((res) => {
            console.log("editAccount -> res", res)
        })
        .catch((err) => {
            console.log("editAccount -> err", err)
        })
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{flex:1}}>
                    <RNCamera
                        style={{flex:1}}
                        type={type}
                        ref={camera} 
                    >
                        <View style={{}}>
                            <TouchableOpacity onPress={() => toggleCamera()}>
                                <MaterialCommunity name="rotate-3d-variant" size={15}/>
                            </TouchableOpacity>
                        </View>
                        <View style={StyleSheet.round}/>
                        <View style={StyleSheet.rectangle}/>
                        <View style={StyleSheet.btnTakeContainer}>
                            <TouchableOpacity style={{}} onPress={() => takePicture()}>
                                <Icon name="camera" size={30} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={[styles.container, {backgroundColor:'#ffffff'}]}>
            <View style={styles.profileContainer}>
                {/* <Image sorce={route.params.photo !== null && photo null ? {uri: `${url}`} : null} /> */}
                {/* <Image sorce={route.params.photo !== null && photo null ? { uri : uri.home + data.photo + '?' + new Date(), cache: 'reload', headers: {Pragma: 'no-cache' }} /> */}
                <TouchableOpacity style={styles.rounded} activeOpacity={0.7} onPress={() => toggleCamera()}>
                    <Icon name="camera" size={15} color={'#ffffff'} />
                </TouchableOpacity>
            </View>
            <View style={styles.detailUser}>
                <View style={styles.editContainer}>
                    <View>
                        <Text style={{}}>Nama Lengkap</Text>
                        <View>
                            <TextInput 
                                ref={input}
                                value={name}
                                editable={editable}
                                style={styles.input}
                                onChangetext={(value) => setName(value)}
                            />
                            <Icon name="edit-2" size={20} color="grey" onPress={() => editData()}/>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.editTitle}>Email</Text>
                        <View style={styles.editItem}>
                            <TextInput 
                                value={email}
                                editable={false}
                                style={styles.input}
                                onChangeText={(value) => setEmail(value)}
                            />
                        </View>
                    </View>
                </View>
                <View style={{marginTop: 30}}>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => onSavePress()}>
                        <Text> SIMPAN</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {renderCamera()}
        </View>
        
    )

}

export default editAccount;