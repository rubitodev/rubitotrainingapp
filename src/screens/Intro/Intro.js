import React from 'react'
import { View, Text, Image, StatusBar, SafeAreaView,StyleSheet, TouchableHighlight } from 'react-native'
// import styles from './style'
// import colors from '../../style/colors'
// import { Button } from '../../components/Button'
//import module  react native app intro slider
import AppIntroSlider from 'react-native-app-intro-slider'

// data yang akan kita tampilkan sebagai onboarding aplikasi
const data = [
    {
        id: 1,
        image: require('../../assets/images/intro-1.png'),
        description: 'Sebarkan Kebaikan'
    },
    {
        id: 2,
        image: require('../../assets/images/intro-2.png'),
        description: 'Kepedulian kita'
    },
    {
        id: 3,
        image: require('../../assets/images/intro-3.png'),
        description: 'Harapan mereka yang membutuhkan'
    }
]

const Intro = ({ navigation }) => {

    //tampilan onboarding yang ditampilkan dalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={styles.listContainer}>
                <View style={styles.listContent}>
                    <Image source={item.image} style={styles.imgList} resizeMethod="auto" />
                </View>
                <Text style={styles.textList}>{item.description}</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <StatusBar backgroundColor={'#3781ff'} barStyle="light-content" />
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>Koin Peduli</Text>
                </View>
                <View style={styles.slider}>
                    {/* contoh menggunakan component react native app intro slider */}
                    <AppIntroSlider
                        data={data} //masukan data yang akan ditampilkan menjadi onBoarding, dia bernilai array
                        renderItem={renderItem} // untuk menampilkan onBoarding dar data array
                        renderNextButton={() => null}
                        renderDoneButton={() => null}
                        activeDotStyle={styles.activeDotStyle}
                        keyExtractor={(item) => item.id.toString()}
                    />
                </View>
                <View style={styles.btnContainer}>
                    <TouchableHighlight style={styles.btnLogin} onPress={() => navigation.navigate('Login')}>
                        <Text style={styles.btnTextLogin}>MASUK</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.btnRegister} onPress={() => navigation.navigate('Register')}>
                        <Text style={styles.btnTextRegister}>DAFTAR</Text>
                    </TouchableHighlight>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Intro;
const styles = StyleSheet.create({
    listContainer:{
        width:400,
        height:400,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    listContent:{
        width:350,
        height:350,
        alignItems: 'center',
        
    },
    imgList:{
        width: 300,
        height:300,
    },
    container:{
        flex: 1,
        justifyContent: 'space-around',
        alignItems:'center',
        backgroundColor: '#3781ff',
    },
    textLogoContainer:{
        width:200,
        
    },
    textLogo:{
        color:'white',
        fontSize:24,
        fontWeight:'bold',
        justifyContent: 'center',
        alignItems:'center',
        marginHorizontal:20
    },
    textList:{
        color:'#ffffff',
        fontSize:14,
        fontWeight:'bold',

    },
    slider:{
        width:400,
        height:400,
        
    },
    activeDotStyle:{
        // width:250,
        color:'#ffffff'
    },
    btnContainer:{
        width:'90%',
        height:100,
        justifyContent: 'space-between',
    },
    btnLogin:{
        backgroundColor:'blue',
        width:310,
        height:40,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: '#1c2869',
        borderWidth:1,
        borderRadius:4,
    },
    btnTextLogin:{
        color:'white',
        fontWeight:'bold'
    },
    btnRegister:{
        backgroundColor:'grey',
        width:310,
        height:40,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderWidth:1,
        borderColor:'#ffffff',
        borderRadius:4,
    },
    btnTextRegister:{
        color:'white',
        fontWeight:'bold'
    },


})
