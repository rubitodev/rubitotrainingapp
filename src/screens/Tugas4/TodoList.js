import React, { useContext } from 'react';
import { SafeAreaView, View, Text, StatusBar, TextInput,FlatList,TouchableOpacity, TouchableHighlight, StyleSheet } from 'react-native'
// import styles from './style';
// import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
import { RootContext } from './Context';

const TodoList = () => {

    const state = useContext(RootContext)
    console.log("TodoList -> state ", state )
    
    const renderItem =({ item, index }) => {
        return (
            <View style={styles.list}>
                <View>
                    <Text>{item.date}</Text>
                    <Text>{item.title}</Text>
                </View>
                <TouchableHighlight>
                    <Icon name="trash" size={25} style={{marginRight:10,marginTop:10}} />
                </TouchableHighlight>
            </View>
        )
    }
    return (
        
        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor="#ffffff"/>
            <Text style={{alignItems:'flex-start', marginTop:20}}>Masukan Todolist</Text>
            <View style={styles.formContainer}>
                
                <View style={styles.row}>
                    <TextInput
                    style={styles.input}
                    placeholder="Input here"
                    onChangeText={(value) => state.handleChangeInput(value)}
                    />
                    <TouchableOpacity style={styles.addButton} onPress={() => state.addTodo()} >
                        <Icon name="plus" size={25} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.listContainer}>
                <FlatList
                    data={state.todos}
                    renderItem={renderItem}
                />
            </View>
        </SafeAreaView>
    )
}
export default TodoList;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent:'flex-start',
    },
    addButton:{
        borderWidth:1,
        backgroundColor:'#008EE2',
        width:50,
        height:50, 
        alignItems:'center',
        justifyContent:'center', 
        marginTop:30,
        marginLeft:5
    },
    formContainer:{
        flexDirection: 'row',
        borderColor: 'black',
        width: '100%',
        justifyContent:'center',
        minHeight: 40,
        marginLeft:10,
        paddingHorizontal:10,
    },
    input: {
      flex: 1,
      borderWidth:1,
      marginTop: 30,
      width:"96%",
      height:50,
      fontSize: 18,
      fontWeight: '600',
      paddingLeft: 10
    },
    listContainer: {
        flex:1,
        marginTop: '5%',
        flexDirection: 'row',
        width: '96%',
        justifyContent:'center',
        height:80,
        paddingHorizontal:10,
    },
    list:{
        flexDirection:'row',
        justifyContent:'space-between',
        borderWidth:0.5,
        marginBottom:10,
        minHeight:50,
        width:'100%'
    },
    row: {
      width:'98%',
      flexDirection: 'row',
      justifyContent:'space-between',
      borderColor: 'black',
      paddingRight: 10,
      paddingBottom: 5,
    }
  });