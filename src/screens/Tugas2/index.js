import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const myIcon = [
  {
    id: '1',
    title: 'Saldo',
    title2: 'Rp.120.000.000',
    name:'money',
    size:30

  },
  {
    id: '2',
    title: 'Pengaturan',
    name:'cogs',
    size:30
  },
  {
    id: '3',
    title: 'Bantuan',
    name:'question-circle',
    size:30
  },
  {
    id: '4',
    title: 'Syarat & Ketentuan',
    name:'edit',
    size:30
  },
  {
    id: '5',
    title: 'Keluar',
    name:'sign-out',
    size:30
  },
];

function Item({ title,title2, name, size}) {
    return (
        <View style={styles.item}>
            <Icon name={name} size={size} />
    <Text style={styles.title}>{title}</Text>
    <Text style={styles.title}>{title2}</Text>
        </View>
    );
}

function Tugas2 () {
  const renderItem = ({ item }) => (
      <View>
    <Item name={item.name} title={item.title} title2={item.title2} size={item.size} />
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
          
      <View style={{width:'100%', height:100, backgroundColor:'#4A95FF',justifyContent:'flex-end' }}>
    <Text style={{color:'#ffffff', fontWeight:'bold', fontSize:20, marginBottom:20, marginLeft:20}}>Account</Text>
      </View>
      
      <View style={{width:'100%', height:90, backgroundColor:'#ffffff', flexDirection:'row',justifyContent:'space-around' }}>
        
      <Image 
            source={require('../../assets/images/foto.jpg')} 
            style={{borderRadius:50, width:70, height:70,justifyContent:'center',marginTop:10}}
            />
            <Text style={{fontSize:20, fontWeight:'bold',marginVertical:30}}>Rubito</Text>
      </View> 
      <FlatList
        data={myIcon}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#ffffff',
    padding:25 ,
    marginVertical: 8,
    marginHorizontal: 16,
    flexDirection:'row',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 16,
    marginLeft:35,
    marginRight:75,
    fontWeight:'600',
    justifyContent:'space-around'
  },
});

export default Tugas2;