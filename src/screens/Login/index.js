import React, { useState, useEffect } from 'react';
import { View,Image, Text,TouchableOpacity, StyleSheet, StatusBar,TextInput,} from 'react-native';
import Axios from 'axios';
import auth from '@react-native-firebase/auth';
import {GoogleSignin, statusCodes, GoogleSigninButton} from '@react-native-community/google-signin'
import Asyncstorage from '@react-native-async-storage/async-storage';
import api from '../../api';
import TouchID from 'react-native-touch-id';


const config = {
    title: 'Authentication Required',
    imageColor : '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

function Login ({navigation}) {
    const [email, setEmail ] = useState('');
    const [password, setPassword ] = useState('');

    const saveToken = async (token) => {
        try {
            await Asyncstorage.setItem("token", token)
        }catch (err) {
            console.log(err);
        }
    }

    useEffect(() => {
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess:false,
            // webClientId: '522655694505-iqmdn47a75hoa8rlbns0g313krr8t5k5.apps.googleusercontent.com'
            webClientId:'162498121718-obvn3lfmlpb3u7lg5t5j8md376vg75p6.apps.googleusercontent.com'
        
        })                
    }

    const signInWithGoogle = async() => {
        try {
            const {idToken} = await GoogleSignin.signIn()
            console.log("signInWithGoogle -> idToken", idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)
            
            navigation.navigate('Profile')
        } catch (error) {
            console.log("signInWithGoogle -> error", error)
        }
    }
    const onLoginPress = () => {
        let data = {
            email : email,
            password : password
        }
        Axios.post(`${api}/login`, data, {
            timeout:20000,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then((res) =>{
            console.log("Login -> res", res);
            saveToken(res.data.token)
            navigation.navigate('Profile')
        })
        .then((err) => {
            console.log("Login -> res", err);
        })
    }

    const signInWithFingerprint = () => {
        TouchID.authenticate('', config)
        .then(succes => {
            
            alert("Authentication Succes")
            navigation.navigate('Profile')
        })
        .catch(error => {
            alert("Authentication Failed")
        })
    }
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#ffffff' barStyle="dark-content"/>
                <View>
                    <Image source={require('../../assets/images/logo-cf.png')} style={styles.imgLogo}/>
                </View>
            <View style={styles.content}>
                <View style={styles.form}>
                    <Text style={{marginTop:100}}>Username</Text>
                    <TextInput 
                        borderWidth={0.3}
                        backgroundColor={'#ffffff'}
                        marginVertical={5}
                        value={email}
                        underLineColorAndroid = "#c6c6c6"
                        placeholder="Username or Email"
                        onChangeText={(email) => setEmail(email)}
                    />
                    <Text>Password</Text>
                    <TextInput 
                        borderWidth={0.3}
                        backgroundColor={'#ffffff'}
                        marginVertical={5}
                        secureTextEntry
                        value={password}
                        placeholder="Password"
                        underLineColorAndroid="#c6c6c6"
                        onChangeText={(password) => setPassword(password)}
                    />
                    <View style={{borderRadius:4,borderWidth:0.5,marginVertical:5, backgroundColor:'#1c2869', 
                            width:310, height:40, justifyContent:'center', alignItems:'center'}}>
                        <TouchableOpacity onPress={() => onLoginPress()}>
                            <Text style={{fontSize:14, color:'white', fontWeight:'bold'}}>Login</Text>
                        </TouchableOpacity>
                    </View>
                        <Text>Belum punya akun ?</Text>
                        <View style={{borderRadius:4,borderWidth:0.5,marginVertical:5, backgroundColor:'yellow',
                            width:310, height:40, justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                                <Text>Daftar</Text>
                            </TouchableOpacity>
                            
                        </View>
                    {/* <GoogleSigninButton
                        onPress={() => signInWithGoogle()}
                        style={{width:310, height:40}}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Dark}
                    /> */}

                        <View style={{borderRadius:4,borderWidth:0.5, backgroundColor:'#3EC6FF', 
                        width:310, height:40, justifyContent:'center', alignItems:'center', marginBottom:40, marginTop:10}}>
                            <TouchableOpacity onPress={() => signInWithFingerprint()}>
                                <Text style={{fontWeight:'bold', color:'#ffffff'}}>Sign in With Fingerprint</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            </View>
        </View>
    )
}
export default Login;
const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'space-evenly', 
        alignItems: 'center', 
        marginTop:90
    }, 
    imgLogo:{
        width:355, 
        height:80, 
        marginBottom:10,
        marginTop:5
    },

})