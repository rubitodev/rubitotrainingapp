import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Task from '../../components/Task';


const TodoList = () => {
  const [value, setValue] = useState('')
  const [todos, setTodos] = useState([])

  handleAddTodo = () => {
    if (value.length > 0) {
      setTodos([...todos, { text: value, key: Date.now()}])
      setValue('')
    }
  }

  handleDeleteTodo = (id) => {
    setTodos(
      todos.filter((todo) => {
        if (todo.key !== id) return true
      })
    )
  }

  handleChecked = (id) => {
    setTodos(
      todos.map((todo) => {
        if (todo.key === id) todo.checked = !todo.checked;
        return todo;
      })
    )
  }
  return (
      
      <View style={styles.container}>
        <Text style={{ marginTop: '10%', fontSize: 16, color: 'black', alignItems:'flex-start' }}>Masukan Todo List</Text>
        <View style={styles.textInputContainer}>
          <TextInput
            style={styles.textInput}
            multiline={true}
            onChangeText={(value) => setValue(value)}
            placeholder={'Input here'}
            placeholderTextColor="grey"
            value={value}
          />
          
          <TouchableOpacity style={{borderWidth:1, backgroundColor:'#008EE2', width:50,height:50, alignItems:'center', justifyContent:'center', marginTop:10, marginLeft:5}} 
          onPress={() => handleAddTodo()}>
            <Icon name="plus" size={25} color="black" />
          </TouchableOpacity>
        </View>
        <ScrollView>
          {
            todos.map((task) => (
              <Task
                text={task.text}
                key={task.key}
                checked={task.checked}
                setChecked={() => handleChecked(task.key)}
                delete={() => handleDeleteTodo(task.key)}
              />
            ))

          }
        </ScrollView>
      </View>
  )
}
export default TodoList;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    marginLeft:15
  },
  textInput: {
    flex: 1,
    borderWidth:1,
    marginTop: 10,
    width:50,
    height:50,
    fontSize: 18,
    fontWeight: 'bold',
    paddingLeft: 10
  },
  taskWrapper: {
    marginTop: '5%',
    flexDirection: 'row',
    borderColor: 'black',
    borderBottomWidth: 0.5,
    width: '100%',
    minHeight: 40,
  },
  task: {
    paddingBottom: 20,
    paddingLeft: 10,
    paddingTop: 6,
    borderColor: 'black',
    borderBottomWidth: 1,
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  textInputContainer: {
    width:'98%',
    flexDirection: 'row',
    justifyContent:'space-between',
    borderColor: 'black',
    paddingRight: 10,
    paddingBottom: 5,
  }
});