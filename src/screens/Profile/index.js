import React,{useEffect, useState} from 'react';
import { View,Image, Text,TouchableOpacity, StyleSheet, StatusBar,TextInput, Button,} from 'react-native';
import Axios from 'axios';
import Asyncstorage from '@react-native-async-storage/async-storage';
import api from '../../api';
import { GoogleSignin } from '@react-native-community/google-signin';


function Profile ({navigation}) {
    const [userInfo, setUserInfo]= useState(null)

    useEffect(() => {
        async function getToken() {
            try {
                const token = await Asyncstorage.getItem("token")
                return getVenue(token)
                console.log("getToken -> token", token)
            } catch(err){
                console.log(err)
            }
        }
        getToken()
        getCurrentUser()
    },[userInfo])

    const getCurrentUser = async () => {
        const userInfo = await GoogleSignin.signInSilently()
        console.log("getCurrentUser -> userInfo", userInfo)
        setUserInfo(userInfo)
    }

    const getVenue = (token) => {
        Axios.get(`${api}/venues`, {
            timeout:20000,
            headers:{
                'Authorization' : 'Bearer' + token
            }
        })
        .then((res)=> {
            console.log("Profile -> res", res)
        })
        .catch((err) => {
            console.log("Profile -> err", err)
        })
    }

    const onLogoutPress = async () => {
        try{
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
            await Asyncstorage.removeItem("token")
            navigation.navigate('Login')
        } 
        catch(err){
            console.log(err)
        }
    }
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#3EC6FF"/>
            <View style={{padding:10, alignItems:'center', justifyContent: 'space-around',flexDirection:'row', marginHorizontal:20}}>
                <TouchableOpacity onPress={() => navigation.navigate('editAccount')}>
                {/* <TouchableOpacity> */}
                <Image source={require('../../assets/images/foto.jpg')} style={{width:80, height:80, borderRadius:50}}/>
                </TouchableOpacity>
                <Text style={{fontSize:14,fontWeight:'bold'}}> Rubito </Text>
            </View>
            <View style={styles.cardContainer}>
                <View style={styles.row}>
                    <Text>Tanggal Lahir</Text>
                    <Text>06 November 1990</Text>
                </View>
                <View style={styles.row}>
                    <Text>Jenik Kelamin</Text>
                    <Text>Laki - Laki</Text>
                </View>
                <View style={styles.row}>
                    <Text>Hobi</Text>
                    <Text>Ngoding</Text>
                </View>
                <View style={styles.row}>
                    <Text>No. Telp</Text>
                    <Text>08778364996</Text>
                </View>
                <View style={styles.row}>
                    <Text>Email</Text>
                    <Text>rubitojogja@gmail.com</Text>
                </View>
                <View style={{}}>
                    <TouchableOpacity onPress={() =>onLogoutPress()}>
                        <Text style={{fontSize:14,fontWeight:'bold', marginLeft:20}}>Logout</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
export default Profile;

const styles = StyleSheet.create({
    row: {
        borderWidth:0.3,
        marginBottom:10,
        paddingVertical:10,
        marginHorizontal:10,
        padding:10
    }


})
