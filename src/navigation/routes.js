import React, { useState, useEffect } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Intro from '../screens/Intro/Intro';
import SplashScreens from '../screens/Splashscreen/Splashscreen';
import Login from '../screens/Login';
import Profile from '../screens/Profile';
import editAccount from '../screens/Account';
import Register from '../screens/Register';

const Stack = createStackNavigator()

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen 
            name="Intro" 
            component={Intro} 
            options={{ headerShown: false }} />
        <Stack.Screen
            name="Login"
            component={Login} 
            />
        <Stack.Screen
            name="Profile"
            component={Profile} 
            />
        <Stack.Screen
            name="editAccount"
            component={editAccount} 
            />
            <Stack.Screen
            name="Register"
            component={Register} 
            />
    </Stack.Navigator>
)

const AppNavigation = () => {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

    },[])

    if(isLoading) {
        return <SplashScreens />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation;
