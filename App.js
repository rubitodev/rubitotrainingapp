import React from 'react';
// import {NavigationContainer} from '@react-navigation/native'
import 'react-native-gesture-handler';
// import Router from './src/navigation/routes';
import AppNavigation from './src/navigation/routes';
import firebase from '@react-native-firebase/app';

// var firebaseConfig = {
//   apiKey: "AIzaSyCrqjYLR-68aPWIfGoxOsbJmkyiDk9vmGM",
//   authDomain: "koinpeduli-ff512.firebaseapp.com",
//   projectId: "koinpeduli-ff512",
//   storageBucket: "koinpeduli-ff512.appspot.com",
//   messagingSenderId: "522655694505",
//   appId: "1:522655694505:web:1d506aa20c18c5201673b8"
// };
// // Initialize Firebase
// firebase.initializeApp(firebaseConfig);

var firebaseConfig = {
  apiKey: "AIzaSyDSmJE98EkqlFF2-zOvo552LGnw4ax9BHY",
  authDomain: "rubitotrainingapp.firebaseapp.com",
  databaseURL: "https://rubitotrainingapp.firebaseapp.com",
  projectId: "rubitotrainingapp",
  storageBucket: "rubitotrainingapp.appspot.com",
  messagingSenderId: "162498121718",
  appId: "1:162498121718:web:6dfc19b47d549c3ed001d7"
};
// Initialize Firebase
// firebase.initializeApp(firebaseConfig);
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App =()=> {
  return (
    <AppNavigation />
    // <NavigationContainer>
    //   <Router />
    //   </NavigationContainer>
  );
};

export default App;
